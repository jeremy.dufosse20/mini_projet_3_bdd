'''
Programme qui lit le fichier csv "out.csv" et stock ses données dans la
bdd "voiture"
'''

import sqlite3
import csv
import sys
import unicodedata
import os.path
import os

bdd_path=('./voiture.sqlite3')
csv_path=('./out.csv')

def cleanRow(row):
    '''
    Si il y a des caracteres speciaux, cette fonction
    va netoyer ligne par ligne le dictionnaire a envoyer a la bdd
    IN: (ligne unclean)
    Out: (ligne clean)
    '''
    cleanrow = row.replace(";"," ").replace("--", " ")
    cleanrow = unicodedata.normalize('NFKD', cleanrow).encode('ascii', 'ignore')
    return cleanrow.decode('ascii')

def dicoBDD(row):
    '''
    Le contenu de out.csv est mis dans un dictionnaire et va etre
    envoyer dans la bdd
    '''
    dicoBDD = {'adresse_titulaire': cleanRow(row['adresse_titulaire']),'nom': cleanRow(row['nom']),
            'prenom':cleanRow(row['prenom']),'immatriculation':cleanRow(row['immatriculation']),
            'date_immatriculation':cleanRow(row['date_immatriculation']),'vin':cleanRow(row['vin']),
            'marque': cleanRow(row['marque']),'denomination_commerciale': cleanRow(row['denomination_commerciale']),
            'couleur': cleanRow(row['couleur']),'carroserie': cleanRow(row['carroserie']),
            'categorie': cleanRow(row['categorie']),'cylindree':cleanRow( row['cylindree']),
            'energie': cleanRow(row['energie']),'places': cleanRow(row['places']),
            'poids': cleanRow(row['poids']),'puissance': cleanRow(row['puissance']),
            'type': cleanRow(row['type']),'variante': cleanRow(row['variante']),
            'version': cleanRow(row['version']),}
    return dicoBDD

def errorBDD(error):
    '''
    Cette fonction retourne la bdd a sa forme d'origine en cas d'erreur
    '''
    connection.rollback()
    connection.close()

def createTable():
    '''
    Creation de la table "data"
    '''
    try:
        connection = sqlite3.connect(bdd_path)
        cursor = connection.cursor()
        cursor.execute('''CREATE TABLE data (
        adresse_titulaire TEXT, nom TEXT, prenom TEXT, immatriculation TEXT,
        date_immatriculation TEXT, vin TEXT, marque TEXT, denomination_commerciale TEXT,
        couleur TEXT, carroserie TEXT, categorie TEXT, cylindree TEXT,
        energie TEXT, places TEXT, poids TEXT, puissance TEXT, type TEXT,
        variante TEXT, version TEXT); ''')
        connection.commit()
        return cursor, connection
    except sqlite3.Error as identifier:
        errorBDD(identifier)

if os.path.exists(bdd_path):
    try:
        connection = sqlite3.connect(bdd_path)
        cursor =connection.cursor()
    except sqlite3.Error as identifier:
        errorBDD(identifier)
else:
    cursor,connection=createTable()

caracteres = dict.fromkeys(c for c in range(sys.maxunicode)
if unicodedata.combining(chr(c)))

if __name__ == '__main__' :
    with open(csv_path) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            iteration = 0
            for col in cursor.fetchall():
                iteration = iteration + 1
            if iteration > 1:
                errorBDD("Une ligne est presente plus d'une fois")

            else:
                cursor.execute('''INSERT into data (adresse_titulaire, nom, prenom, immatriculation,
                    date_immatriculation, vin, marque, denomination_commerciale, couleur,
                    carroserie, categorie, cylindree, energie, places, poids, puissance,
                    type, variante, version) values(:adresse_titulaire, :nom, :prenom, :immatriculation,
                    :date_immatriculation, :vin, :marque, :denomination_commerciale, :couleur,
                    :carroserie, :categorie, :cylindree, :energie, :places, :poids, :puissance,
                    :type, :variante, :version)''', dicoBDD(row))

    connection.commit()
    connection.close()